<?php

namespace IAGC\RedirectSimpleToConfigurable\Plugin\Catalog\Controller\Product;

class View
{
    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $configurable;

    /**
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable
     */
    public function __construct(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable
    ) {
        $this->configurable = $configurable;
    }

    /**
     * Product view action
     *
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\Controller\Result\Redirect
     */
    public function aroundExecute(
        \Magento\Catalog\Controller\Product\View $subject,
        \Closure $proceed
    )
    {
        $productId = (int) $subject->getRequest()->getParam('id');
        $parentIds = $this->configurable->getParentIdsByChild($productId);
        $parentId = array_shift($parentIds);

        if($parentId) {
            $subject->getRequest()->setParam('id', $parentId);
        }

        return $proceed();
    }
}
